# Obvious disclaimer

These are my personal notes from 2020 IoT course. I passed with 30/30 cum laude, but ofc I cannot guarantee anything is correct so use at your own risk.

# Editing

Notes are written in markdown and compiled into pdf (through Latex) by using `pandoc`.
I have included a makefile, therefore if you have it in your PATH, along a pdf engine (default is `pdflatex`, edit makefile if needed), you can edit sources and recompile simply by using `make`.
