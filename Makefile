dest=IoT.pdf
src=src.md

$(dest): $(src)
	pandoc $(src) -o $(dest) -s --table-of-contents -V documentclass=report --number-sections -H math.tex
clean:
	rm -f $(dest)
