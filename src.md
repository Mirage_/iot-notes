---
title: Notes on Internet of Things
author: Andrea Castelllani
date: Spring 2020
---

# Wireless systems

## Analog and digital signals

**Analog signal**: an electric signal varying in time and intensity. It is obtained by translating a physical signal, while retaining the original shape of the physical signal. It can then be transmitted directly, or encoded into a digital signal and then transmitted.
If transmitted through analog means, the quality of the received signal is expressed through **Signal-to-Noise ratio**, which represents how degraded the signal is compared to the original form: 
$$S/N = 10\log_{10}\frac{Signal}{Noise}$$

**Digital signal**: a sequence of timed electric impulses which can take a limited number of values (typically, only 2 for binary transmissions). The time intervals are provided by a **clock**, a signal with period equal to the duration of a bit, used by both transmitter and receiver; they must be synchronized such that bits are always red in the same instant, which is typically in the middle of the bit period to minimize distortions.
After synchronization is achieved, bit can be read by comparing the value of the received signal (in each clock period) with a given *threshold* (typically at half intensity) such that any read above will be a `1`, and any below, a `0`.
Because of this mechanism, it is not actually necessary to preserve as much as possible the exact shape of the transmitted signal, as long as the noise is not high enough to cause a misread, resulting in a *flipped* bit. Quality of received signal is therefore measured by **Bit Error Rate**: 
$$BER = \frac{\text{\# of wrong bits received}}{\text{\# of total bits received}}$$
By measuring the BER, it may be possible to switch to a different modulation technique (possibly at the cost of lower data rate) in order to be able to transmit at a given S/N.

## Differences with wired systems

Transmission medium is different from traditional wired networks over TCP/IP, with several consequences:

- **broadcast** nature of the medium, i.e. transmission can be intercepted/overheard within transmission radius $\rightarrow$ security challenges
- **high bit error rate** $\rightarrow$ needs mechanisms for reliability 
- **collisions**, if not managed, can prevent reception or degrade the signal at receiver $\rightarrow$ need specific **Medium Access Control** (MAC) protocols
- requires support for **mobility**

For these reasons, they require dedicated **data link** & **transport** layers protocols.

In wired TCP/IP, when packet loss occurs, systems infer network congestion and thus decrease transmission rate. In wireless transmissions however, this is often causes simply by collisions or other errors in transmission, therefore the correct response is usually not to throttle the data rate but simply retransmit the packet. 

Another problem for many devices is **power management**, since recharging may be time-consuming and expensive. Possible solutions being researched:

- energy efficient protocols
- mobile device offloading
- wake-up radios

There are two main types of wireless networks:

- **Infrastructure networks**: wired infrastructure connected to wireless devices.  
- **Ad Hoc wireless network** (i.e. P2P model): all devices are similar and can act either as source/destination or as relay to route traffic. 
Can also include a gateway or *sink*, which can connect the entire wireless network back to Internet. 

**Hidden terminal problem**: two (or more) devices may transmit simultaneously outside each other's detection range, but overlapping on a third node, creating a collision there. Simply retransmitting will not solve the problem, therefore dedicated arbitration is required from MAC protocols.

**Routing**: must be dynamic to account for nodes moving, being turned off (e.g. out of battery) or malfunctioning.
Efficient routing algorithms may also optimize traffic to prevent or **minimize routing** non-direct traffic **through low-power nodes**, in order to maximize the time they stay functional.


## Ideal signal propagation

Medium phenomena such as:

- **Diffraction**: the wave is bent by a sharp edge of some encountered surface
- **Scattering**: wave is spread into multiple different directions by an object that is smaller than the wavelength
- **Reflection**: wave is reflected with a mirrored direction
- **Shadowing**: wave is blocked or reflected away from the intended destination by some large object, such that destination does not receive it at all

Can cause the signal to reach the destination through different paths than directly as expected (i.e. line of sight), potentially causing different frequencies to arrive at slightly different times.
However, these phenomena can also be exploited to potentially reach devices *not* actually in **line of sight**, which is what makes transmissions possible also in urban environments.

Additionally, radio frequencies from *omnidirectional* antennas follow **geometrical spreading** over distance, which causes exponential **signal attenuation** which implies a reduction in **signal to noise ratio** and thus quality. In particular, the power density at distance $d$ is equal to the ratio between transmission power $P_T$ and surface area of a sphere centered in the source with radius $d$:

$$F_{\tiny omni} = \frac{P_T}{4\pi d^2} \quad \small(W/m^2)$$

To reduce the waste of power, when possible *directional* antennas are used instead, which spread the signal over a smaller surface, resulting in higher power at destination compared to omni-directional antennas.
This is represented in two measures:

$$\text{Directivity:}\quad D = \frac{\text{power density at distance $d$ in direction of maximum radiation}}{\text{mean power density at distance } d}$$

$$\text{Gain (dB):}\quad G = k  \frac{\text{ power density at a distance $d$ in direction of maximum radiation }}{P_T/ 4\pi d^2 \equiv F_{\tiny omni} }$$
$$\small (k<=1:\text{antenna efficiency factor)}$$

- omnidirectional antennas are simpler
- directional antennas have higher effective range in the desired direction through reduced spread
- directional antennas also benefit from reduced interference from other signals

This means that the power density at distance $d$ for directional antennas is modified by maximum transmission gain $g_T$:
$$F_{direct} = \frac{P_T g_T}{4\pi d^2}$$
  
Additionally, receiver will almost always receive the signal over an area rather than a single point, therefore formula must be adjusted by an additional gain at receiver $g_R$.
Accounting all this, power received ad distance $d$ in case of direct transmission (no obstacles) can be expressed by **Friis transmission equation**: 

$$\text{Receive Power (dBm):}\quad P_R = P_T g_T g_R \left(\frac{\lambda}{4\pi d}\right)^2 L^{-1}$$ 
$$\small (\lambda: \text{wavelength},\quad L>1: \text{hardware losses})$$

This can be used to show that GSM can transmit up to ~35Km.

- **dB**: ratios between powers are expressed according to a logarithmic scale $(10\log_{10}P_1/P_2)$ called **Decibel**. A ratio of $10dB$ therefore implies an order of magnitude between two powers, i.e. $P_1 = 10 P_2$ ^[$10\log 1/0.1=10$. If $P_1 = 2 P_2$, ratio is $3dB$].
- **dBm**: ratio between a given power and a nominal power of $1mW$ (milliwatt)

**Path loss** (dB): transmit power / receive power. Depends on both distance and frequency $(c/\lambda^{-1})$ ^[speed of light over wavelength]:

$$PL(d) = (\lambda/4\pi d)^{-2}$$


## Multipath models

In cases of multiple paths, the received signal can be both attenuated or amplified depending on many factors.

**Two ray model**: in typical city environment, when transmitting to a destination in LoS there will usually be signal arriving directly and signal arriving through reflection, since transmitting antenna will usually be at height $h_t$ greater than receiver's $h_r$. In this case, received power decreases much faster with distance (factor of 4) than in the **free-space model** (which can approximate countryside):

$$ P_R = P_T g_R g_T \left( \frac{h_t h_r}{d^2}\right)^2$$

A **generalized** form of *Friis* transformation equation can still be used to represent **mean received power** as a function of the power decrease factor $2\le \eta \le 5$ of the propagation environment:

$$P_R = P_T g_T g_R \left(\frac{\lambda}{4\pi}\right)^2 \frac{1}{d^\eta}$$ 

**Rayleight fading**: signal received through several non-direct paths, typical of densely populated urban areas. The signal received at time $t$ is equal to the combination of the components, but it oscillates according to the phase of the components, with the following power distribution:

$$
f_p(x) = \frac{1}{2\sigma^2}e^{\frac{-x}{2\sigma^2}}
$$

This can cause the signal to intermittently fall below the receivable threshold, meaning that starting at a certain distance (below the maximum theoretic range) we will have a *probability* of not correctly receiving (**outage probability**).

Additionally, if signal replicas suffer significantly different delays due to attenuations, they can arrive at the time allocated for a subsequent symbol and therefore cause **symbol interference**. This impact can be quantified by measuring the root mean square of the delay (**RMS delay spread**).
This kind of interference can be attenuated through techniques like **equalization**, which consists in transmitting in a specific pattern in order to be able to recognize and filter this type of interference at destination.

# Energy efficiency

Some devices potentially needed to be able to operate for years without access to power recharge, and for this reason rely on external sources of energy (batteries, solar cells).
Better battery technologies are unlikely to solve this problem, therefore power consumption is a key element in designing wireless networks.

- A conservative approach is the idea that a node going offline may compromise the performance of the whole network, therefore it is may be critical to design ways to minimize power consumption at low power nodes to maximize their uptime. 
- However, this is often excessive, in many networks there is some kind of redundancy and therefore the target is having less than a given % of node offline - which man better balance the load than preventing power loss in a certain node at huge power markups for the rest of the network.

Energy efficiency is measured in terms of energy spent per bit delivered, but note that traditional metrics (throughput/latency) are still relevant for real-world use, therefore energy efficiency cannot be optimized for in isolation, i.e. trade off exist and must be considered on application basis.

- Dynamic voltage scaling


## Network related consumption

- computing
- communications
  - transmit electronics
  - transmit power amplifier
  - receive electronics

> Where to place the computations?

Earlier means power expense on potentially lower power devices, but allows to transmit less/more optimized data, potentially saving power in the long run.

- Adapting **power control**: reduce transmission power gradually for as long as the receiver can ACK transmissions, in order to transmit at the minimum power required by the specific time/place.
- **Minimizing transmission time**, because nodes in sleep mode often consume very little. This may make faster but otherwise less energy efficient transmissions worth it depending on specific costs & gains.
- **Promiscuous mode** could be used to learn routing information for free by "copying" neighboring traffic paths, but this requires passing to higher layer also packets not directed to the node, which usually is *not* worth the power cost, as maintaining **deep-sleep** whenever possible is more efficient.

For **long range** transmissions, the highest percentage of power consumption is due to the power amplifier, therefore power control is key.
For **short range**, the higher cost is the transmit electronics (i.e. exiting low power mode), therefore minimizing transmission time is key.

**Transceiver states**:

- **tx**: awake, transmitting
- **rx**: awake, receiving
- **idle**: awake, not active
- **asleep** (possible more than one mode)

Switching mode takes a few milliseconds and *does* have a power cost (as well as potentially latency), therefore constant switching must *also* be prevented.

Common MAC: **awake/asleep schedule** to minimize awake time. Tends to increase latency.

- **synchronized**: nodes are programmed to wake up at certain times, following a **duty cycle**. Very efficient in theory, suffers from gradual desync over time, meaning periodic readjusting is needed (costly). Receiver typically consumes more than sender while awaiting transmissions.
- **asynchronized**: nodes do not know neighbor schedule, transmitting nodes keep transmitting an ACK request until receiver confirms he is awake and receiving. High up-front cost, but no overhead costs.

Additional measures to reduce wastes:

- minimize collisions
- minimize packet headers: compression, transmitting only diffs...
- delay transmissions rather than transmit over a bad channel
- if **energy harvesting** is an option, exploit full-power nodes which have **excess** energy (they could harvest but not store more), in order to relay packets at effectively no cost.

## Historical data

In the 2000s, tx was significantly more costly than rx. Nowadays, for low-to-medium range rx power costs are comparable to tx, or even higher (awaiting information that is not actually arriving), and **idle time** is nearly as expensive as active transmissions.

Implementation details can be surprisingly impactful:

- In 3G nearly 60% of energy consumption was due to **tail energy**, i.e. energy consumed right after the end of transmissions. This turned out to be because of a bad *implementation* choice: switching back off the transceiver was done through a timeout, wasting a lot of precious milliseconds.
- In GSM, power consumption was much lower for small packages due to significantly lower tail energy, but with increasing packet size (higher transmission cost = less overhead in %), the advantage would decrease and eventually disappear.
- WiFi would outperform both technologies in either scenario

Possible mitigations:

- Combine 3G with wifi when available
- Try to batch transmit delay-tolerant applications (e.g. mail)
- Energy-optimized pre-fetching techniques for web-surfing

While higher data rate (e.g. larger packets) increases power consumption, it resulted in net gains due to reduced transmission time. 
On the other hand, increasing frame-rate was shown to increase overall power consumption significantly, meaning there is considerable cost in crossing then protocol stack.


# Cellular systems

Hexagon model with **base stations** at each center, since real transmission surfaces (circles) have to be overlapping to cover all the space (simply assign user to closest station if in overlapping zones).
It is important to use a proper cluster where frequency **reuse distance** is maintained between cells that use the same channel, so that no interference happens while the number of required channels remains tractable.

## Resource allocation

Problem of resource allocation to multiple users, as well as multiple technologies: due to very high numbers, producers have to pay high fees to reserve certain frequency bands. Therefore, in addition to frequency reuse, resources are multiplexed with various strategies:

- **FDMA**: Frequency Division Multiple Access (channel-based): allocate portions of the frequency band to users
- **TDMA**: Time Division Multiple Access (time-based): allocate time slots to users

In either case, no collisions but each user only gets $1/|\text{users}|$ of the total bandwidth.

- **CDMA**: Code Division Multiple Access, can accommodate multiple user at same time & frequency. Each user is assigned a specific code (transmitted data is data x code, called *chipping sequence*), which allows the users to transmit with minimal interference if their codes are orthogonal and arrive with similar power. The signal can then be decoded by inner product of encoded signal and sequence.
- **OFDMA**: Orthogonal Frequency Division Multiple Access, to maximize sharable frequency space. 
  
### OFDMA 

To maximize sharable space, each sub-carrier must be orthogonal (i.e. to minimize **inter-carrier interference**), then the actual transmission is an inverse Fourier transform, so that the destination can then decode it through a normal Fourier transform. Sub-channels at the edge of the band are not modulated to avoid leakage outside the frequency band, in order not to need guard bands.

Multi-path fading will however introduce an **inter-symbol-interference** due to combinations of received signals from different paths. To prevent this from degrading the signal, OFDMA uses **cyclic prefix** (?). A side advantage is that signal acquisition interval does not need to be very precise anymore.

Additionally, ODFMA symbol can be exposed to frequency selective channel (...?)
Part of the sub-carrier are always dedicated to act as *pilot* for the symbol, which can help in recovering the signal distorted by multi-path fading because, since the pilot's position is fixed, the receiver can adjust based on where (in the spectrum?) the received pilots are.

Problems:

1. peak-to-average-power ratio: when subcarriers are added, instantaneous power will be more than the average, which can cause non-linear distortions as well as spectral fading, due to going into the nonlinear region of the amplifier at the transmitter. This results in out-of-bands radiations as well as power inefficiency.
2. Carrier frequency offset (?)

Advantages:

- Splitting channel into narrowband enables simplification of equalizer design
- Scalable numbers of subchannels allows for flexible bandwidths
- Possible to exploit both time and frequency domain variations.



## GSM (2G)

- Second-generation cellular system
- Transmissions are digitally encoded,  8 channels per carrier, each one with 200KHz bandwidth
- Each 200KHz channel is further split in time-slots assigned to 8 different users (i.e. **FDMA+TDMA**)
- Data rate of ~270 Kb/s per channel 
- Uses Gaussian Minimum Shift Keying (GMSK)
- Supports power control
- **Discontinuous transmission**: to save power, voice transmission is interrupted during speech pauses (which are very frequent)
- **Frequency Division Duplexing** (FDD): lower part of frequency band is used for **uplink**, higher part for **downlink** ^[**uplink**: to base station, **downlink**: from base station], because transmission at higher frequency consumes more thus better reserved for the base station.

Each TDM Frame is 4.615 ms, thus each slot is 577 $\mu$s long (enough for ~156 bits) and is mainly meant to carry voice-transmission, encoded at 13 kbit/s. More modern encodings use Variable Bit Rate (VBR).
Uplink and downlink communication happen in different slots (i.e. with an offset) of the respective frequency band.


### Network Architecture

Components:

- User Equipment (UE), also called Mobile Station (MS)
- Radio Access Network (RAN)
  - BS
  - other components
- Core Network (CN)
- External networks

There are also MS that can transmit over different frequency bands, or multiple slots per frame, in order to increase data rate (e.g. used for GPRS).


**Radio subsystem**:

- **Mobile Station** Subsystem (MSS)
  - ME (Mobile Equipment, i.e. a cellphone): has a unique international identifier (IMEI) and contains transceiver
  - **SIM** (CPU + memory) contains the logic required to connect to the cellular network
    - serial number to uniquely identify SIM card 
    - International Mobile Subscriber Identity (IMSI), uniquely identifies user
    - temporary network information such as LAI and TMSI (temporary MSI, changed frequently for privacy, and linked to location) 
    - keys necessary for encryption

- **Base Station** Subsystem (BSS)
  - **Base Transceiver Station** (BTS): hw/sw components that allow to exchange physical information with MEs
    - TRX (**transceiver**)
      - modulation, coding, multiplexing, error correction, encryption
    - BCF: **base common functions**
      - frequency hopping, if enabled
      - synchronization
    - channel quality measurement, reported along with MS's to BSC
    - broadcasts system information messages needed by MSs to access the cell's network
  - **Base Station Controller** (BSC): performs resource management of a group of BTS.  Usually includes TRAU.
    - manages BTS configuration
    - involved in user authentication
    - allocates resources (channels) to initiate calls.
    - decides on handovers
  - **Transcoder Rate Adaption Unit** (TRAU): performs transcoding from GSM into PCM, the higher rate code used for the rest of the network. Each GSM carrier (8 channels at 13Kbps) requires 2 PCM channels at 64Kbps + a control channel (LAPD).

(NSS)

  - Mobile Switching Center (**MSC**)
    - connection management (CM)
    - mobility management (MM)
  - Virtual Location Register (**VLR**): contains info on users currently located in the area
  - Home Location Register (**HLR**): contains info on users that have signed a contract with the provider. Contacted when someone is trying to reach one of its numbers, as it also contains which VLR to contact to reach the requested number.
  - Authentication center

**Operation and maintenance subsystem** (OMSS): units responsible for monitoring, maintenance, remote management, accounting & billing. A hierarchical system of OMCs (operation & maintenance centers).

Areas:

- **MSC/VLR**: area managed by a MSC, which will store relative info in its own VLR.
- **Location Area**: each MSC/VLR zone is further divided into 1 or more LAs with their own identifier (LAI). Whenever a user moves between LAs, a location update has to be sent.
- **Cell**: area served by a base station. Each LA usually has several cells.


### Security measures

- Authentication: uses 128 bits keys stored in SIM.
- MS sends its IMSI to identify itself, in the VLR each IMSI is associated with a TMSI.
- TMSI is changed with each location update
- Equipment Identity Register (EIR): database containing info on terminal equipments that can be used to protect network from stolen or non-standard equipment

[...skipping some details...]

### Frequency hopping

Originally developed for security, this technique is still used because switching channels over a larger frequency band (according to a pseudo random sequence computed by both the sender and receiver) allows to mitigate the risk of attenuations or interference, by spreading them over multiple information flows.

### Interleaving

When transmitting with **error correction** code, a certain % of bit flips can be corrected, recovering the packet. However, often interference is very temporary, but strong enough to make a few packets unrecoverable.
To combat this, instead of transmitting full packets in order, an often useful technique is to divide the packets in blocks, and interleave them with block of subsequent packets. In this way, when the packet is reconstructed at the receiver, if a transmitted packet is lost, the actual data lost is spread over several packets, often causing a recoverable amount of bit flips. Therefore, rather than drop one bad packet, the receiver has a good probability to receive packets that are all either intact, or recoverable through error correction. 

### Logical channels

**Logical channels** are virtual channels mapped to different physical resources (GSM frame slots).
In addition to data channels, several control channels are needed for control information: connection setup, synchronization, paging etc.

- Common, i.e. information to/from multiple users (multiplexed over time)
  - BCH: **broadcast control** channel, for information of general use, only **downlink**
    - FCCH: **frequency correction** channel, to correct MS frequency
    - SCH: **synchronization** channel, carries BS identity code and frame number
    - BCCH: **broadcast control** channel, carries other general information (LA code, parameters for freq. hopping and more) 
  - CCCH: **common control** channels, accommodate multiple users over time but information is meant for single users, for information not needed frequently enough to warrant a dedicated channel
    - PCH: **paging** channel, downlink. Used to notify a BS of a call to a certain MS and locate it. Broadcasted over LA.
    - RACH: **random access** channel: uplink, used by MS to request access to network (location updates, call requests). Prone to collisions, resolved by waiting a random time before retransmitting (if ACK not received within timeout window) ^[it is a **slotted Aloha** protocol, which is simple and good enough for most real cases, but introduces significant delays and not very efficient in heavy congestion]
    - AGCH: **access grant** channel, downlink, carries reply to RACH requests.
- Dedicated channels, i.e. channels assigned to single users
  - DCCH: **dedicated control** channels
    - SDCCH: **stand-alone dedicated control** channel, assigned after RACH for authentication, call set-up, and other services (e.g. SMS)
    - SACCH: **slow associated control** channel, bidirectional, used to exchange connection metrics and power control commands. *Slow* because it is multiplexed with normal traffic (1 slot per 26 frames, i.e. 950 bit/s)
    - FACCH: **fast associated control** channel, for time-critical information (e.g. handover request). *Fast* because it will take over resources usually dedicated to traffic.
  - **traffic** channels
    - **THC**: full rate, 13Kbps net, 22,8Kb/s after error correction coding
    - **THC/H**: half rate
    - TCH/E: enhanced full rate

### Mapping

**Mapping** to physical channels is done according to slot IDs, with a **multiframe** structure: a given sender will split its data between all the slots with the given id over multiple frames, as well as multiple frequencies if frequency hopping is enabled. Signaling does not require a whole slot due to smaller bit rate needed.
In uplink, most control channels are multiplexed into slot 0 of each frame. In downlink, first slots are used for FCCH and SCH (repeated each 10 frames), then BCCH (once per 51-frame multiframe), rest is for the various CCCHs, plus 1 slot for 8 SDCCH.
Frame #25 in each multiframe is used as a search frame, i.e. to monitor BCCH of current and adjacent channels. 

The information actually transmitted in each slot is called **block**. Sent through several techniques:

- normal burst: for transmissions over traffic channels
- access burst: for RACH. This has longer guard periods because timing advance has not been setup yet.
- frequency correction burst: for FCCH, used to lock MS to BTS's local oscillator (?)
- synchronization burst: for SCH
- dummy burst: random bits through unused BCCH slots, so that MSs can estimate channel quality

### synchronization

For GSM to work, MS must retrieve precise frequency of radio carrier, then must synchronize with frame and slot structures in order to be able to receive both normal and control transmissions.

1. MS scans frequency band for the SCHs run by BTSs, which periodically transmit at high power a sequence of bits
2. MS selects the best available BTS for the given location.
3. MS listens to SCH for the required multi-frame information, to FCCH for frequency correction and to BCCH for other info
4. **Timing advance**:
   Even after learning slot structure, propagation delays have to be accounted for to achieve slot synchronization. Therefore, a guard period between slots is required to avoid interference from other slots. This delay would be equal to the round trip time, which given the maximum range of ~35km, would at its worse be 233$\mu$s (which means wasting ~68 bits). To limit the waste, BTS will instead estimate delay from the MS, relay the observation to it, and the MS will subsequently anticipate transmissions by the measured time in order to arrive at the BS at correct time. When user moves significantly, this process is repeated. This allows GSM to achieve ~33$\mu$s guard band instead (~9bits). 

### Voice coding

At transmitter:

1. filtering
2. sampling
3. coding
4. compression

At receiver:

1. reconstruction
2. decoding
3. expansion

[some physical details on voice coding]

For higher efficiency, there is an **adaptive predictor** which modifies the data to be sent so that only the delta with the predictor is sent. Receiver will use the same predictor to obtain original information.


### Mobility management

The smaller the cell radius, the less energy is consumed for transmissions, as well as resulting in a higher number of users over the whole system (more efficient frequency reuse); however, this requires higher infrastructure cost (more BTSs) as well as more overhead due to mobility management. 
Additionally, cells are grouped in **location areas** (LA). Small LAs will cause frequent location updates (mobility management overhead), while large LAs will increase paging overhead.

#### Idle

When a mobile terminal is turned on, it will select its cell according to the synchronization process. The MS must register its location with the current LA's VLR (location update), which in turns informs the user's HLR of the new position so that the user can be contacted. The HLR will also request deletion of user data from previous VLR. Finally, in case of **roaming**, the user will be notified of the operator of the current network. 

While in **idle** mode, the MS will keep periodically scanning the signal received from the supported frequencies, and switch base station if a better signal is received from a different one.
Additionally, a user moving to another LA will trigger a **location updates**, meaning that the operator's DB will be updated with his new LA so that calls can still be routed to him.
When a MS is switching off, is sends a *IMSI detached* message, which informs the BS to stop paging until MS signal it is back on.

#### Paging

When a call arrives, the user's operator is queried for the user's current LA, then the network initiates a procedure where each BS in the specified LA will broadcast a control message with the requested user ID. When the MS answers the message, the network will know which BTS it should route the call to.
Paging is not done towards **detached** users. 

#### Handovers

If a user moves to another cell while they are currently in an **active** state (i.e. in a call), then there is a need for an **handover** ^[or *handoff*] from the old to the new base station: this means that a new channel must be allocated for the transmission in the new cell, the old channel must be deallocated, and the active connection must be routed to the new channel in a seamless way. In GSM, this process is always **network-assisted** (i.e. managed by the network) rather than the MS, although it uses information collected by both the MS and BTSs.

Handover must be decided *before* the signal drops under the critical threshold, else the call will suffer, but not *too* much above it, else too many handovers will be performed (e.g. a user moving back and forth in a small area may cause continuous handovers).
There are several methods:

- strongest signal: simplest, but prone to the above *ping-pong effect* because handover often happens before a user actually crosses into another cell, making it likely that the user may move back enough to trigger an handover to the previous cell shortly after.
- strongest signal, but only switch when previous signal is below a threshold (critical + some safety). This strategy aims at minimizing handovers.
- strongest signal, with *hysteresis*: switch when another cell's signal is stronger than current signal by a given factor $h$. This strategy aims at maintaining high radio quality at the cost of more handovers than previous mode.

**Problem**: destination cell may not have a channel available for the incoming user, causing the handover to be rejected. Additionally, that cell will also have to block new incoming calls.
Since maintaining active connections is more important than accepting new calls (dropped calls are perceived much worse than failed calls by users), some **guard channels** are reserved for serving handovers, though this means a higher chance of dropping an incoming call (*Pblock*) in order to decrease the chance of rejecting an handover (*Pdrop*), potentially causing a call to be dropped.
If no channels are available anyway, the handover will be queued but the user will still be served by his old base station for as long as possible (i.e. above critical threshold).
In the worst case, GSM may switch some channels to **half-rate encoding** to accommodate more users in a cell at the cost of voice quality.

Handover procedure should be transparent to the user, which usually implies <100ms.

Handovers may also be initiated by BSTs for other reasons beside radio channel quality:

- MS's distance from BTS over a threshold: even if channel quality is still fine, may require excessive timing advance
- current cell is congested and need to offload traffic
- maintenance of the cell

4 types of handovers:

- **intra cell** (intra BSC): simple handover decided by a single BSC. A new traffic channel is allocated, often frequency is modified. Used when TCH registers low quality but signal strength is still high (usually this means that TCH is suffering from some interference).
- **intercell**/intra BSC: handover controlled by a single BSC when a user is moving to another one of its cells. BSC identifies a better suited BTS and allocates a TCH there, releasing the old one. MS then uses SACCH to acquire info on neighboring cells. If it is a new LA, location update is triggered.
- **inter BSC** (intercell): user is moving to a cell of a different base station controller. BSC identifies best suited BTS and request a connection there to MSC. New BTS creates a TCH and informs MS to switch to it, using routing provided by MSC.
- **inter MSC**: user is crossing into another MSC/VLR zone. Current BSC asks MSC to request handover to a certain BTS in destination MSC. Destination MSC replies with a **handover number** [^hon] and creates a connection to new BSC (which allocates a TCH). Initial MSC sends handover command to MS using FACCH of previous BTS.

[^hon]: Like MSRN and MSISDN numbers, **HON** = Country Code + National Destination Code + Subscriber Number. SN points to a database (HLR for MSISDN, VLR otherwise). This is enough information to route calls.

In 3G, **soft handovers** were introduced, which allow a terminal to be connected to multiple base stations simultaneously.

### Call setup

Traditional phone network call *to* MS:

1. number is analyzed by PSTN/ISDN network, routed to GMSC (general message switching center) of the operator of the called number
2. GMSC request routing information from HLR
3. HLR identifies address of user's current MSC/VLR zone
4. HLR asks user's **roaming number** to MSC/VLR
5. User's current MSC/VLR allocates a mobile station roaming number (MSRN) for the incoming call and sends it to requesting HLR
6. GMSC routes the call to user's current LA
7. User's current LA locates user through **paging**
8. The target MS replies to its current base station by requesting a SDCCH channel through RACH
9. MSC/VLR activates authentication and cyphering
10. A TCH is allocated for the call
11. User is notified of the call and can answer.

**Traffic channel setup** for calls *from* a MS:

1. RACH request (MS $\rightarrow$ BS)
2. AGCH reply which carries SDCCH assignment ($\rightarrow$)
3. traffic signaling + TCH assignment over SDCCH ($\leftrightarrow$)
4. TCH + SACCH traffic exchange ($\leftrightarrow$)


## From 2G to 4G

Most improvements have been made using an evolutionary approach, i.e. building on top of previous systems & infrastructures, while more radical changes are are still in discussion for 5G.
There was a need to increase data rates (for more applications other than just voice) as well as increase quality of voice encoding.

### 2G+

First solution was **2G+**: 

- **GPRS** (General Packet Radio Service) allows MSs to also send data (i.e. non-voice), through IP packets.
  - This requires routers: in backbone, **GSNs** (GPRS Support Nodes)
  - In BSS, PCUs (Packet Control Units): deals with resource allocation for IP packets (segmentation, scheduling, error detection, MAC, channel management...)
  - [various new control channels...]
- Support for MS receiving multiple slots per frame, for higher data rate when channels are readily available
- Location services: can triangulate MS location within 100m by using BTSs
- Number portability between operators
- SIM app toolkit: SIM can now request ME to perform commands, call numbers and more
- **EDGE**: new type of radio access technology which uses **8PSK modulation**, triple the data rate per symbol (now ~59 Kbit/s), but less robust
  - Adaptive MultiRate: variable bitrate voice encoding depending on channel conditions. If S/N is low enough to cause high BER, switch to (old) GMSK modulation
- Since a MS switching on is likely to need to send or receive information, MS now stay for a period in a **ready** state in which the location is known by cell (thus no paging needed). 
  - After a period, they go to **standby**, in which its position is tracked by **Routing Area** (RA), a subsection of the LA. 
  - After a timer or MS detach, go back to **idle** (tracking by LA, reachable by paging).

### 3G

This generation was the first to overcome data rate limitations of 2G, reaching Mbps (much closer to Wifi networks).

- 5Mhz per channel
- TDD and FDD
- Support for asymmetric services
- uses **CDMA** (Code Division Multiple Access)

### 4G LTE (Long Term Operation)

4G (2005) uses different technologies for uplink and downlink communications.

- increased data rate
- uniform service provision (previously, service was worse at cell edge)
- improved **spectral efficiency**, i.e. bits per Hz (by now, spectrum is nearly completely allocated)
- reduced latency
- reduced energy consumption for MSs
- seamless mobility, requiring larger number of BTS also in harder-to-reach locations (ideally, support trains)
  - includes low complexity receivers (eNB) capable of using either 2G, 3G or 4G
- first architecture directly meant for data (packet switching), rather than voice and extended to accommodate data
- uses **OFDMA** (Orthogonal frequency-division multiplexing) for downlink, symbols are grouped in **resource blocks** (RBs, 180 kHz, 0.5ms)
- a user is dynamically allocated a number of slots, and will have a proportionally high modulation and thus data rate
- attempt to also compete with WiFi for home networks, but never used in practice


Simplified architecture:

- User Equipment (UE)
- Evolved UMTS Terrestrial Radio Access Network (E-UTRAN)
- Evolved Packet Core (EPC)
  - S-GW: Serving Gateway, transports user data to external networks
  - P-GW: PDN Gateway, maintains info on QoS, does IP allocation to UE, allows interconnection to different cellular networks
  - MME: Mobility Management Entity, does connection set-up, paging, security operations (comparable to switching center)
  - HSS: **Home Subscriber Server**, maintains information on users (combines HLR and authentication center)


## The 5G vision

Mobile access does not meet current demands of QoS, i.e. reliable ultra-high broadband, not only for the use of humans, but also IoT devices. To accomplish this, several paradigm shifts are in discussion for 5G (2014+).

Original White paper vision ^[link?]:

- 1000x total throughput, 10x speed to individual users
- latency down to 1ms
- 90% increase in energy efficiency, 10x battery lifetime for low-power devices
- real full coverage, including planes & other transportation
- new, service based business model
- privacy integrated in design
- Energy efficiency, since ICT currently uses 5% of global energy consumption, and to allow widespread use of low-power devices

Challenges:

- QoS across different use-cases
- Simplicity
- Multi-tenancy across different infrastructure ownerships
- Density due to IoT devices
- Harnessing: exploit any communication capability there is, including *device-to-device*
- Resource management
- Identity management
- Precise localization (sub meter)

Extend frequency band: up to hundreds GHz (**millimeter wave**) to increase data rate and number of supported devices.

Several technologies use OFDMA, other more recent ones like LoRaWAN use ultra-narrow band.

### Cellular IoT

The idea is to provide connectivity to IoT devices through the pre-existing cellular infrastructure, but introducing ad-hoc radio interfaces: same physical layer, update software of base stations.
This can be done either in stand alone bands, inside licensed bands, or inside guard bands thanks to the narrow band technologies.

Advantage of this method is low cost installation, while exploiting extended coverage made possible by small IoT devices (e.g. networks underground, submerged etc.). 
As always, however, power efficiency is a key aspect. Additionally, for licensed bands, this implies that performance guarantees cannot be made because there is always the possibility of interference.

Main types of technologies:

- EC-GSM-IoT
- LTE-M: LTE with reduced band and extended power saving mode
- NB-IoT: Narrow band.

#### NB-IoT

Architecture is the same as LTE, except for end devices.

Two modes for uplink:

- single tone 15 kHz and/or 3.75 kHz tone spacing
- multi tone 

Three coverage classes:

- Normal
- Robust
- Extreme

Repetitions achieves extra coverage (up to 20 dB compared to GPRS), at the cost of spectral efficiency.

Overall, performance achieved strongly depends on deployment scenario and configuration parameters.


# Ad-hoc networks

Ad-hoc networks are **wireless multi-hop infrastructure-less networks** whose devices act both as source/destination as well as **relay** for packets meant for other nodes. This allows for lower overall costs and can reach remote places, but requires high degree of self-organization between the nodes. 
Often, an ad-hoc networks are used for deploying a **sensor network**, which then forwards all data to the Internet through a **gateway**; other applications include military networks, disaster recovery, home networking, WiFi extension through mash.

The biggest problem is devices have very **limited resources**, both power and storage, therefore efficiency is paramount and duty-cycling is a necessity. This implies that the network has to be able to **self-adjust** whenever a node goes offline or wakes up; for some applications, **mobility** of nodes must also be accounted for. Because of power efficiency, however, overhead costs must also be low or the solution will not be able to **scale** to most real-world uses.

There are several primitives for communication, depending on the network:

- **Broadcast**: by *flooding*, i.e. each node forwards to all nodes except the origin of its received packet.
- **Converge-casting** : from all/many to one.
- **Unicast**: typically from sink to a node, some networks also support node-to-node unicast

Ad-hoc networks use approaches like **CSMA/CA**, but not CSMA/**CD** ^[csma-cd] like ethernet, because of medium access control problems: nodes cannot receive and transmit at the same time, but **collision detection** is not always possible due to **hidden terminal** problem. Therefore **collision avoidance** must be used instead: before transmitting, send **RTS** (request to send) to see if the channel is being used. If it is, wait till end of transmission + random (exponentially increasing) backoff timer. If it isn't, destination should be able to answer with a **CTS** (clear-to-send).
RTS also includes **NAV** (Network Allocation Vector), which has a fairly precise estimate of the time-allocation requested for the channel.

While TDMA is a possibility, since ad-hoc networks do not have a central system this is much harder to implement, but it is used for sensor networks.

^[csma-cd]: uses collision detection techniques: listen to the channel while transmitting, if a collision is detected, abort transmission. 

## Routing

### Bellman-Ford

Traditional routing algorithm, computes shortest path between 2 nodes in a given graph with weighted edges (non-negative).

### Distributed Bellman-Ford

Each node receives traversing estimates from neighbors, computes own cost by adding 1, forward to its own neighbors. Continue to propagate for as long improvements are registered: at the end, each node should converge to the best estimated route.

Problem: **loops**, particularly **count to infinity** *("bad news travel slow")*. If a node disappears, paths through it increase to infinity. Updating whole network of this takes a long time because nodes exchange the value of their best path to other nodes, but now what this path is (so neighbors can't know whether this path is still valid at a given time).
Therefore, only suited to small networks.

- Use a TTL and discard updates from too far away.
- For small diameter of $x$, a cost of $x+1$ is considered equal to infinity.


### Proactive algorithms

Characteristics:

- based on traditional distance-vector and link-state protocols
- each node still maintains route to each other network
- periodic and/or event-triggered routing update exchanges

Limitations:

- high **overhead** in most cases
- cost of maintaining routes updated may not be worth it for some scenarios (e.g. high mobility)
- longer **convergence time**

  
#### Distributed asynchronous Bellman-Ford (DSDV)

- uses Dynamic Destination-Sequenced Distance-Vector (DSDV) routing.
- **fresh routes** (i.e. recently updated)  are *preferred*, as they are much more likely to have currently valid information
- **shortest-path is only a tie-breaker** for fresh routes
- identify fresh routes by **sequence number**, which increases with each update (only at original source of update)
- broadcast updates as in DBF, in incremental dumps, but attach sequence number
- offline nodes are detected by neighbors when not receiving a **periodic "hello" message**, or when frame transmission to node repeatedly fails. Cost increases from 1 to inf, send updates to neighbors with new sequence number (+2, not +1, to force a route update even if another node is publishing an update around the same time). - Neighbors receive new seq number thus immediately adjust.
- newly detected routes (inf-> finite) are immediately advertised
- improved routes are only periodically advertised, as even better routes could be received shortly after


#### Optimized Link State Routing (OLSR)

- suited for large and dense networks
- decrease overhead caused by flooding by identifying only a small subset of nodes (**multipoint relays**, *MPR*) in charge of forwarding routing information
- each node selects its MPR among its one-hop neighbors that can reach all 2-hop neighbors
- greedy construction of the $\MPR(X)$ set

[? details of implementation]

[in distributed async BF we consider seq number of source of an update. What is seq number of forwarding node used for?]

### Reactive algorithms

- routes are built ***on-demand***, by flooding
- only maintain active routes
- less overhead
- better scaling properties

Main limitation: **route acquisition delay**.

#### Ad-hoc On-Demand Distance Vector Routing (AODV)

- reactive version of DSDV, key aspect is still freshness of route
- nodes only maintain routes to a destination if they need to reach it (wether for transmitting or forwarding another node's packet)
- route are computed **recursively**

**Route discovery**:

  1. S need to route to D
  2. Creates a route request (RREQ) with $IP_D$, $seq#_D$, $IP_S$, $seq#D$, `hopcount=0`, broadcast ID
  3. nearest neighbor $A$ receives RREQ:
     1. Make **reverse route** entry for S (will be erased after a timeout window if no RREP is received)
     2. IF no route to D, forward RREQ to neighbors with `hopcount+=1`
  4. each node that receives RREQ maps reverse route to forwarding neighbor, and keeps forwarding
  5. eventually, a node $C$ that has a path to $D$ receives the RREQ
     1. $C$ creates route reply (RREP)
     2. unicasts RREP to the neighbor that forwarded the RREQ
  6. going backward through the path, each node maps route to each node in the forward subpath and then forwards backwards using the reverse routes it recorded previously
  7. IF $S$ receives RREP within a timeout window, makes forward route to $D$ with `nexthop=`$A$ and hopcount contained in the RREP

Additionally:

- if a node receives a better RREP, update its forward route and forward it backward up to the source of the RREQ.
- each packet sent through a route refreshes its timeout window, if it runs out route is considered inactive and deleted from memory
- if a link breaks, the immediate neighbor in the backward path invalidates the path by sending a RERR backward.
-  Each node that receives a RERR tries to find an alternative route:
   - IF it finds it, send update backward (?).
   - ELSE forwards RERR backward  

#### Dynamic Source Routing (DSR)

- unlike AODV, active routes include route record (i.e. the whole path), not just the nexthop
- seq# are not used
- RREQ includes the route record, which is recursively built until destination
- no need to store reverse routes since RREQS include the whole path
- discard `<initiatior ID, request ID>` if already seen recently
- to avoid loops, discard RREQ if receiving node is already in route record

Optimizations:

- extensive caching can be used (recency-based, no need for lifetime), but requires **promiscuous mode** to be useful (i.e. discover routes by listening on neighbors' traffic)
- a node that knows a route to a destination can immediately answer a RREQ with **the full path**. This could however cause a *broadcast storm* if several or all neighbors have requested route, time-based duplicate detection mechanism is required to prevent this.
- **unsolicited RREP**: ?

In practice, not very widespread because promiscuous mode is too power consuming.

### Geographically-enabled routing

If each nodes knows its current position (proper hardware is required), this may be used in computing routes.

**Location-enabled ad hoc routing**: 

- Scalability is an issue, because nodes have to maintain estimates of other nodes' positions.

**Location-Aided Routing** (LAR):

- uses geographic information to aid reactive algorithm through **greedy forwarding**: only forward to neighbors in a certain area, with preference for those closer to the requested destination.
- problem: needs to know where the destination is, or at least its general direction

**DREAM**:

- directional routing
- instead of greedy forwarding, use **directional flooding** to nodes in a cone region towards the destination
- use location updates to account for potential mobility of destination node: closer nodes require more update, further less updates due to cone shape.
- faster nodes send more updates, slow or immobile nodes have low overhead

#### GeRaF

- cross-layer: MAC and routing
- Destination (a sink) is assumed static
- nodes are duty cycled
- need to forward packet towards destination only using active nodes
- greedy forwarding: selects relays that is closest to the final destination
- each node only knows own position and destination, plus position of incoming transmission's source (included in header)
- low overhead because nodes do not need to know or synchronize with other nodes' awake-asleep schedule

1. RTS invites all awake neighbors to act as relays
2. active nodes respond with CTS
3. node select the best one (closest to destination) according to the reported position

**MAC problem**: there could be collisions if multiple node answers with CTS from same area
**GeRaF solution**: when collision is detected, nodes flip a coin to determine whether to retransmit, until they are successful

In real implementations of GeRaF, instead of using coin-flips, node transmit with a jitter proportional to how suitable the relay is for the source (i.e. nodes closer to destination answer first)

**Dead end problem**:at some point, no forward-path may be available towards destination (e.g. all nodes that could offer positive advancement are off).
A first possible idea is to attempt some number of back-hops (**backtracking**) in hope of discovering another path that is active, but (?)

**Planarization**: an approach to resolve dead-end problem that attempts to *planarize* the network graph and then walk the face perimeter. While theoretically sound, actual implementations proved extremely inefficient.

#### ALBA *(Adaptive Load Balancing Algorithm)*

- provides MAC, geo-routing, load-balancing, mechanisms vs dead-end problem


## Low power MAC protocols

Main sources of energy waste:

- **collisions**: any discarded packet causes retransmission thus more expense. They also increase latency.
- **overhearing**, i.e. picking up packets meant for other nodes
- **idle listening**, i.e. waiting to receive traffic that is delayed or not incoming at all
- control packet **overhead**

Developing a good protocol for Medium Access Control is therefore crucial.

### S-MAC *(Sensor MAC)*

- awake/asleep **duty cycle** of time $d$
- schedule is **synchronized**, periodic resync messages to prevent *clock drifts*
  - **overhead** may be excessive for networks with sporadic transmissions
  - time slots are split in SYN and DATA portions
- all nodes attempt to transmit in **same slot** to minimize duty cycle time, but higher risk of collisions and lower bandwidth
- can have **high latency** due to each hop having to wait for scheduled wakeup

**Setup process**: need to agree on a schedule

- each node waits a random time for a SYN message from neighboring nodes
  - if received within time window, it will follow that schedule (**follower node**). After a random delay, rebroadcast accepted schedule.
  - else, it will select and broadcast its own schedule (**synchronizer node**)
  - if a node receives a neighbor's schedule (followed by  2+ devices) *after* selecting its own, it will follow *both* schedules (more energy consuming). This allows the node to "bridge" between subnetworks using different schedules.
- each node maintains a table with schedule of known neighbors (**schedule table**)
- maintain sync by periodically resending SYN messages
  - also to deal with any collision in setup, or new nodes.

When awake, nodes follow **CSMA/CA**:

- carrier sense for random interval
- if channel clear, send RTS
- if CTS received (virtual carrier sensing), send data
- wait for ACK

Packets are queued and transmitted in burst to reduce **handshake overhead** (RTS/CTS).
Hidden node problem caused by unknown nodes waking up is mitigated by limited packet size and use of ACK, if nodes waking up wait a limited period before transmitting.

### T-MAC *(Timeout MAC)*

- idea: active time should be dimensioned based on traffic need. In S-MAC frame and active time are fixed, in T-MAC active time is dynamically adapted
- sinc like S-MAC
- attempts to **minimize idle** time by going back to sleep *before* scheduled time if no incoming transmission is received within a smaller time window (**aggressive sleep**)
  - timer reset when RTS/CTS/DATA is received/transmitted or collisions are detected
  - aggressive timer must be long enough to receive at least the start of an RTS packet (contend time + actual transmission)
- vulnerable to **early sleep** problems:
  - collisions could cause unwanted early sleep, therefore nodes will attempt transmissions at least twice if RTS initially not answered
  - a target node could go to sleep before the node that wants to reach it can acquire the channel, if it is currently occupied by another node. 
    1. To prevent this, nodes can use **FRTS** *(future RTS)*: special packet to signal nodes that there will be incoming traffic as soon as the sender is free to send , meaning that receiver should stay awake. To prevent FRTS to collide with DATA packets from the currently transmitting node, nodes use dummy packets after CTS to allow neighbors time to send FRTSs to their intended target, while keeping the channel reserved and their own destination awake.
    2. If a node receives an RTS while having a nearly full buffer of packets to send, immediately send RTS instead of answering with CTF (prefer Tx to Rx, original node will reschedule due to no answer)


### B-MAC *(Berkeley MAC)*

- opposite approach: **asynchronous**
- ideal for systems with **sporadic transmission** that do not want the high overhead of synchronizing schedules
- attempts to minimize waste due to collisions through a **clear channel assessment** *(CCA)*
  - **LPL** *(Low Power Listening)*: periodically sample and estimate channel noise to determine whether it is free, by searching for any outlier *below* average noise level in any of the samples
- before transmitting, send **preamble** long enough to wait for *all* sleeping nodes to wake and scan for activity
  - need to account for worst case since schedule of neighbors is unknown

**Problems**:

- if target node wakes mid-preamble, it has to wait for the preamble to end, wasting time
- **overhearing**: other nodes may wake up and wait for preamble to end before finding they were not the target of the Tx
- long preamble had to be implemented through series of short packets anyway, due to architecture of packet radio networks 

### X-MAC

- similar to B-MAC, solves most long preamble problems
- **strobed preamble**: sequence of short messages with only target info, sent with pauses to allow for receiving early ACKs
  - nodes can **early ACK** to signal they're awake, to signal to skip the rest of preamble and proceed to data
  - other nodes can read any short preamble, discover they are not the target and go back to sleep
- shifts most of the burden to the sender

### WiseMAC

- synchronous
- to reduce resources used to sync, nodes attach their own schedule to ACK when they are contacted
- previous sender can use this info to transmit again with better precision
  - to account for clock drift, transmit time window actually used will be slightly wider by some amount (start earlier, end later). Estimated drift of 0.8s/day for realistic clocks.


# IoT standards

There is no single standard for IoT, because standardization started with MAC protocols and moved to network layer only after innovations in the industry done by various different (sometimes proprietary) technologies. This also caused the creation of more than one of them, each focusing on the needs of a specific kind of devices. 

Additionally, the rise of popularity of IoT brought also the already established WLAN technologies such as WiFi and Bluetooth to start testing new low-power variants (BLE, low power WiFi). In fact, low power WiFi is where several new technologies have started industrial testing, since the higher data rate allows for shorter duty cycle which may in some cases allow for better power saving overall (due to maximizing sleep time) even though Tx may cost more.

Most standards involve the gateway eventually connecting to the traditional Internet architecture, sometimes making use of traditional protocol stacks (TCP/IP), sometimes through proprietary software platforms, often deployed in the Cloud.

## 6LoWPAN

**Vision**: connect PANs to Internet through a gateway, but try to fit an IP stack also in low power devices to make them network addressable.

To accomplish this, similarly to how WiFi replaces Ethernet phisical and data link layer (includes MAC), 6LowPan stack replaces PHY and MAC with 802.15.4 PHY and MAC layers, then has LoWPAN on top of MAC (still datalink layer), then has IPv6 for network layer, utilizing UDP or modified TLS. Application layer may be HTTP or others.

- non-routable **local addresses** to have the bare minimum overhead in header (**EUI-64** identifier, 8 bytes) while transmitting in the local subnet
- IPv6 addresses of each device are obtained by concatenating network address with EUI-64

**Packet structure**: 802.15.4 MAC header, mesh addressing header (only if not one-hop transmission), fragment header (only if part of a fragmented frame), IPv6 compressed header, IPv6 payload. 

- headers are not fixed size to save space whenever information can be **compressed**
- **HC1 header** (compressed IPv6 header): all information that can be inferred of the IPv6 header is not transmitted or extremely shortened (no version, traffic class & flow set to 0 represented by a single flag, payload length is inferred, next header is specified only if different from standard)
- **HC2 header** (compressed UDP header): restricted to a subset of possible ports unless noted


Routing modes:

- **mesh-under**: layer-2 forwarding (data link) using local addresses
- **route-over**: layer-3 forwarding (network) using IP addresses
- there is also support for extremely low-power devices (**storage-free mode**) that cannot route: ask PAN coord. for source routing

### IEEE 802.15.4

- ISM 2.4Ghz (16 channels, 250Kbps), 868MHz (single channel, 20Kbps) bands, not licensed but occupied by a large number of technologies (e.g. WiFi), meaning resources are not guaranteed and interference is likely
- **PHY layer functions**, to mitigate the above problems:
  - Energy Detection (ED), 
  - Link Quality Indication (LQI), 
  - Clear Channel Assessment (CCA) 
- **MAC layer**: MAC header, payload, MAC footer

Only supports some topologies:
  - **star**: all end devices connected to the **PAN coordinator** (=sink)
  - **mesh**: devices connect to each other, and potentially eventually with a PAN coord.
  - **tree**: some devices connect to PAN coord. directly, other to Co-ordinator which connect to the PAN coord.

Elements:

- PAN coordinator does net ID assignment, frequency selection, handles join request and does packet relaying. It is chosen through an **election** mechanism.
- Co-ordinator only perform packet relaying and handle join requests of neighbors.
- Nodes entering network perform an active scan to discover the coordinator, then send an association request, which is ACK-ed by PAN coord.

Two operating modes for MAC:

- **beaconless mode**: CSMA/CA based, with CCA (wait random delay, sample channel, if busy retry after increasing delay)
- **beacon mode**: utilize PAN coord. to manage the network. Uptime is divided in several segments:
  - Periodically, coord. broadcasts a *network beacon* packet containing general network information (frame structure, sync info etc.) for the nodes. Nodes wake up at each network beacon, then go back to sleep if they don't need to Tx or Rx. 
  - beacon extension period (reserved space for network beacon)
  - **contention access period**: nodes may transmit (competing for the channel) using CSMA/CA. They may request some slots in the contention free period
  - **contention free period**: several guaranteed time slots reserved by nodes that require guaranteed bandwidth.

Types of packet frames:

- Data: 6LoWPAN-IPv6 frames
- ACK frames
- MAC command frames (association, synchronization, etc)
- Beacon frames, used only by coord. to structure the communication 

A technology implementing this standard is **Zigbee**: covers network, transport and application layer, on top of 6LoWPAN.

**Shortcomings** of 812.15.4:

- designed in 2003, before widespread IoT deployment
- it *does* address **energy efficiency**, but in some cases not explicit or subject to interpretation (e.g. tree topologies)
- does *not* fulfill needs of applications that require **low latency** (e.g. factory automation) or **reliability**
  - no delay guarantees
  - no resilience to interference (no frequency-hopping)
  - MAC not ideal for high-traffic scenarios

#### CTP *(Collection Tree Protocol)*

- anycast route to the sink(s)
- distance vector approach
- **reliability** is key since link quality changes rapidly, CTP achieves >90% delivery rate (avg 98%).
- **ETX** metric: Expected Number of Transmissions to reach the sink i.e. an estimation of *end-to-end link quality* propagated to neighbors
- instead of SEQ number, small cache to recognize duplicates
- flag in frame structure to signal local congestion to nodes requesting forwarding
- count-to-infinity problem resolved by ETX: if metric does not decrease when advancing in the route, a loop has been detected, recompute routing

### IEEE 802.15.4e

To address the specific needs of these applications, in 2012 and 2016 **extensions** were published:

- **Low Energy** (LE): allows devices to operate on <1% duty cycle while appearing to be on to upper layers
- **Enhanced Beacons** (EB): allow to create application-specific beacons
- flexible frame format
- MAC performance metrics
- Fast association (at the cost of more energy)
- **BLINK mode**: *Radio frequency identification blink mode* supports ID exchange
- **Asynchronous Multi-Channel Adaptation** (AMCA): multi-channel use in beaconless networks


- **Deterministic and synchronous Multi-channel Extension** (DSME)
  - supports time-critical applications in large beacon-enabled PANs
  - Core standard already allowed for Guaranteed Time Slots (GTS), but only up to 7 slots, single-channel
  - DSME enhances GTS allowing to construct a multi-superframe, a cycle of superframes, each including beacon frame and GTS, plus the CAP for the first superframe in the cycle.
  - still no frequency hopping
- **Low Latency Deterministic Network** (LLDN)
  - supports very stringent low-latency guarantees.
  - multi-channel extension
  - slotted, beacon-enabled frames
  - star topology
  - short (8-bit) addresses
- **Time Slotted Channel Hopping** (TSCH)
  - slotted access
  - multi-channel support, over different frequencies
  - frequency hopping support, may blacklist detected low quality channels
  - topology-independent
  - low duty-cycle support
  - (?)


#### RPL (*ripple* routing protocol)

- used in both IETF and ZigbeeIP stack
- goals: be robust, long-lasting, self-managing, scalable, energy efficient with low overhead
- built on top of **6LowPan**
- generalization of CTP
- uses **converge-casting** primitive to send traffic towards the edge router, through a **destination-oriented acyclic graph** (DODAG) built using **ICMPv6** control messages (on top of IP)
  - the root (ER-lowPAN border router) sends a **DIO** (*DODAG Information Object*)
  - neighbors receive DIOs, select their parent based on some metric, and if router-enabled, also propagate the DIO
  - may also select additional "reserve" parents
  - each node informs the parent of its choice through a **DAO** (*Destination Advertisement Object*), which is then forwarded up to the root
- **point-to-point** communication is supported using the DODAG 
  - **storing mode**: by optionally storing routing tables in the nodes, so each node can also route back to its leaves
  - **non-storing mode**: if nodes do not support routing table storage, send traffic to sink which will route them back to destination
- whenever an inconsistency is detected, rebuild DODAG and reset timer (?)
- uses a flexible metric for parent selection
- *some* mobility support, at least a set of nodes have to be static

## 6Lo

- Created after completing work on 6LowPan, still not commercially available
- Aims to bring 6LowPan to other network topologies

Technologies included:

- IEEE **802.11ah** (2017) i.e. low-power wi-fi
- Zigbee
- BLE (Bluetooth Low Energy, introduced in 4.0): 
  - faster device discovery over dedicated channels (not used for data)
  - allows mash-topology by combining piconets
  - very low-duty cycle
- LoRa
- Sigfox
- Dash7

### LPWAN

Long range can be achieved by:

- increasing transmit power, not desirable
- using narrowband waveform, done by **Sigfox**
- using **spread spectrum** for coding a bit with a large bandwidth (?), done by **LoRa**

#### LoRa

- Low Power Wide Area network (LPWAN) technology
- Low cost of ownership and positioning
- Outdoor, indoor and **deep indoor** connectivity
- Low power consumption
- Large coverage, high data rate related to area (but low absolute rate)
- maximum transmission power 14dBm (25mW)
- possible bandwidth: 125/250/500 kHz
- 8 channels
- 6 different **spreading factors**

Applications:

- agriculture
- asset management
- smart city
- smart buildings (safety & security, deep indoor penetration)

Downsides:

- very **weak reception** levels $(< -140dB)$
- very **low bit rates** $<1 kbit/s$
- **interferences** from other services when non-licensed bands (ISM) are used
- additional interference at BS due to exposed antennas
- **hidden node** problem precludes the use of CSMA

Typical architecture:

- a base station in highly exposed site
- star of small cost-efficient sensor nodes around BS, communicating with low power ultra-long transmissions (typically several kms, ~7km in city environment with LoS)^[some experiments in space, with a different band, reached 600km]

LoRa uses a patented physical layer ^[developed by Cycleo, acquired by Semtech in 2012] and an open MAC layer on top (**LoRaWAN**)^[standardized by **LoRa Alliance**]. Physical layer uses a variation of **chirp-spread spectrum** which is robust against interference and multipath fading effects.
It uses linear variation in frequencies, i.e. multiple symbols transmitted with different **spreading factors** (i.e. **different duration**), with increasingly longer travel time and lower data rate but also lower SNR limit and thus longer range. The symbols are then transmitted using **circular shift**.

Symbols using different SFs are orthogonal in theory, but only if perfectly synchronized, which is never the case, therefore there is always *some* **inter-symbol interference**. Usually, collisions between packets at a given SF result in correct reception of the strongest signal and loss of the other.

Even in case of collision, due to **capture effect** one of two packets sharing SF is received instead of 0 (higher power). If packets have different sf, due to **imperfect orthogonality** 1 is received (instead of both).

Demodulation can be done up to 20dB *under* noise level (SF 12):

- multiply received signal by raw *down-chirp*
- resulting signal is made of 2 periods, each with a constant frequency ($d$)
- the signal is downsampled at the chip rate (BW Hz?)

**LoRaWAN**: communication protocol and architecture that uses LoRa physical layer

- End notes are connected to 1 or more gateways (LoRaMAC)
- gateways are connected to a network server through various technologies (3G, ethernet...)
- network server is connected (TCP/IP+SSL) to application servers: it mediates all connections between devices and applications

Table: LoRa End devices

class | desc | use case | latency
-|----|--|--
A|Each uplink transmission is followed by 2 short downlink receive windows|battery powered sensors|downlink only after TX
B|A + extra receive windows at scheduled times|battery powered actuators | latency in downlink
C|Continuous receive windows (except when transmitting)|main powered actuators| no latency for downlink

**Gateways** receive on all channels at all times. Low cost as all complex logic is left to network server.

**Network server** is responsible for identifying duplicates, data validation and multiplexing/demultiplexing between app providers, and all decisions on network configuration.

LoRaWAN **frame**:

- physical header
- physical payload
  - MAC header
  - payload
- more...

**Security**: asymmetric encryption (AES-128) in two layers:

- network side: authenticate users, message integrity check
- application security: encrypt data to hide it from network operators
- configuration can be static (manual) or use OTAA

**Over-The-Air-Activation** (OTAA): 

- a `join` procedure required prior to participating in data exchanges with the network server
- must be repeated if session is terminated or lost
- required info:
  - JoinEUI: global app ID using IEEE EUI64 address space, uniquely identifies join server
  - DevEUI: global id of the device in IEEE EUI64
  - AppKey: AES-128 key specific for the end device (only this node is compromised if leaked)
  - NwkKey: AES-128 specific to end device but provided by network operator

**Performance evaluation**

1. **Single** GW:
  - basically a pure aloha system, very limited efficiency (max 18%)
    - no synchronization between transmissions, TX is immediate (while active, duty cycle max 1% by regulation)
    - in case of collision, cancel transmission (ACK are rarely used in LoRa, so no rescheduling)
  - assumptions & notation: 
    - frame arrival rate, from very high number of devices, is Poisson-distributed
    - fixed length frames
    - $T\equiv$ packet time
    - $S\equiv$ **throughput** or **efficiency** i.e. #successful transmissions/packet time
    - $G\equiv$ avg number of **total frames** transmitted / packet time
  - Vulnerability period (time where there is risk of collision) $= 2T$ as there will be overlap between 2 packets in this case
  - Probability of $k$ transmissions at time $t$, by Poisson distribution laws:
 
$$P_k(t) = \frac{(\lambda t)^k e^{-\lambda t}}{k!}$$ 

Since $\lambda 2T = 2G$:

$$S = G\left[\frac{(2G)^k}{k!}e^{-2G}\right]_{k=0} = Ge^{-2G}$$

Which maxes at 18% for $G = 0.5$


When using multiple SFs, performance is *lower* than pure Aloha due to imperfect orthogonality

**Conclusions on scalability**:

- single LoRa cells cannot sustain high-load
- should maintain duty-cycle under 10% per channel
- should opportunistically manage SF and transmission power

2. **Multiple** GWs:
   - performance in case of collisions can greatly improve with well-placed gateways, as each GW may capture a different packet. 
   - With $M$ gateways, system performs close to sum of $M$ separate systems with $G/M$ load.

3. **Adaptive Data Rate**: select smallest possible SF for each given SNR to maximize data rate
   - Done suboptimally by LoRa standard as it is based only on distance from GW and does *not* account for the number of nodes with a given SF
   - Proper **load balancing** would suggest that lower SFs should be assigned to more nodes since it has **faster transmission**.
   - Additionally, **uniform spreading** is superior to assigning SF based on distance i.e. in *circular rings*, because the latter will have the furthest node suffer the most interference instead of spreading the chance and allowing for more capture opportunities.

4. **Power control**: this actually *worsens* the performance because it reduces power across the board, reducing the chance for captures


**Possible optimizations**:

- unfeasible to dynamically configure nodes per-device by network server, too much downlink bandwidth required
  - could use probabilistic setting to have each node choose SF with given probabilities according to RSSI range
  - could add support for broadband configuration commands
- **interference cancellation**: since the strongest signal is correctly captured when there is interference, it could theoretically be possible to cancel the strongest signal in order to recover the weaker one
  - hard to estimate frequency and time offsets precisely
  - receivers cannot handle complex logic if they are to remain cost-effective


# Visible light communication

Video streaming accounts for nearly half of traffic volume on the Internet, and poses stringent requirements on access networks, which brought the necessity for FTTC and FTTH.
Ethernet cables could be used, but nowadays wireless is preferred because it is easier to install and more comfortable to use. However, wireless technologies are close to saturation of the spectrum.

Optical solutions, such as **Infra-red** (Ir) and particularly **Visible Light Communication** (VLC) are promising alternatives:

- VLC spectrum (from 400 THz to 790 THz) is 10.000 larger than RF spectrum (up to 70 GHz)
- it does not currently require any license
- possibility of **spatial reuse** as well as **inherent security** since light does not penetrate walls
- good resistance to multipath fading
- *not* actually limited to LoS (**diffused light**)
- predictable direction of communication (**field of view**)
- no electromagnetic interference (can be used on planes, hospitals etc.)
- overall, **reasonable performance** with **cheap hardware**

VLC has similar frequency ranges to Ir, but Ir has safety problems for transmissions at high power (which requires lasers).
On the other hand, LEDs are already in use for illumination: the idea would be to improve the technology to eventually support data communication.

## VLC history

VLC has actually already been used to relay some limited information for centuries, through mirrors, fire or lamps (e.g. morse code between ships).
In 1880 Alexander Graham Bell invented the *photophone*, which could communicate up to a few hundreds of meters by using sunlight. However, this area of research was eventually abandoned because radio frequencies had much longer range. Additionally, for years scientists were not able to create blue LEDs, blocking further developments until its invention in 2014.

## LEDs

- first lighting system, the **incandescent bulb**, was extremely inefficient (5% light, 95% heat)
- **fluorescent lamp** obtained 25% efficiency and 10.000 hours of life
- solid-state Light Emittind Diode (**LED**) reached 50% efficiency, 50.000+ hours lifespan as well as being much more **compact**

A LED is the composition of two *semiconductors*, one positively charged and one negatively charged ( *opposite doping*). This forces positive and negative charges to move in opposite directions, causing electrons to release energy in the form of light (**photons**) to combine with *holes*.

There are several types:

- **blue chip** + **phosphor**: cheap low-complexity LED that emits blue-to-white light, depending on ther amount of phosphor. Popular for general lighting.
- **RGB**: three different LEDs in parallel. Higher bandwidth through **wavelength division multiplexing** (WDM) or OFDM.
- **OLED** (Organic Light Emitting Diode): uses an organic layer between positive and negative carriers. Lower bandwidth and lifetime than inorganic LEDs, expensive. Used for flat panel displays.
- **Micro LED**: AlGaN-based **micro-light**: larger bandwidth, expensive, same use as OLED.

VLC must also comply with **illumination requirements** if installed in a real room (average & uniform illumination).

Power transmission of an LED varies with the direction (**Lambertian law**).
**Non-linear transfer function**: output optical power is linear with **forward current** times the efficiency of the LED: $P_{opt} = \eta_{led}E\{I(t)\}$ ^[???]

## Reception

Receiver captures light (photons) and converts it into an electrical signal through:

- **photodiode** (PD): a diode operating in **reverse bias** (i.e. photocurrent ~ diode current, meaning changes due to illumination can be detected).
  - PIN PD: suitable for environments with high intensity light
  - Avalanche PD: suitable for environments with weak light intensity, sensitive to temperature changes
- cameras, which don't require multiplexing techniques 
- other LEDs, which can reduce interferences by using narrow band
- solar panels, which can also harvest energy

Depending on application, can use one or multiple receivers. Photocurrent generated is proportional to received optical power and the PD's responsitivity: 

$$I_{photocurrent} = \eta_{pd} P_{opt,rx}$$

It can be improved by using optical concentrators at receiver, or filters against interference in the desired band.

The receiver also needs a a **pre-amplifier** because the current generated by the PD is low:

- high impedance: low thermal noise $\rightarrow$ high sensitivity, low bandwidth, could compensate with an equalizer
- low impedance: high bandwidth, but thermal noise may become predominant
- trans-impedance: converts input current to a proportional output voltage, balance between sensitivity and bandwidth


## Direct modulation

- data is used directly to modulate the electrical current driving the light source.
- simple and cheap
- can be used both for coherent and non-coherent optical sources
- only modulate intensity, which can be done very quickly $\rightarrow$ positive real values only (use e.g. On-Off Keying)
- direct detection with photodetectors
- low data-rate if using LEDs (non-coherent), as inter-symbol interference prevents high rates

Es: **Li-Fi**, very short-range alternative to Wi-Fi.

Communication models:

- **LOS**: beam is wide enough to cover a large portion of the room. Good coverage, high optical losses.
- **narrow LOS**: covers a small portion of the room. Limited coverage and limited losses.
- **quasi diffuse**: transmitter points towards ceiling to reach terminals through reflection of the beam. High losses, very large coverage.
- **diffuse**: no direct LOS between TX and RX, communication through reflection like quasi-diffuse

**Problems**:

- communications when lights are off (e.g. daytime) is power consuming
- flickering
- interference by sunlight (but most of the noise can be filtered out)
- maintaining VLC performance under **link dynamics** is challenging compared to WiFi ^[e.g. a person walking into the room can cause very high noise for a short time]
  - can be mitigated through **SNR prediction** (link SNR variance is high, but stable first and second-order statistics). Collect statistics through full-duplex: whenever a packet is received, send back SNR measurement to estimate best data rate for next packet
  - could use VCL in downlink but WiFi for uplink to work around orientation problems
- **Lighting densification**: high number of light sources could create interference
  - this may be resolved by using high number of smaller cells, possibly with **handovers**. This can also increase comfort for the user (more uniform illumination). Es. `DenseVLC`, requires waveform synchronization (e.g. through non-LOS pilot signals).


## Other Modulation Techniques

**LED-to-LED**

- Transceiver uses a *single* LED for both TX and RX
- can potentially **transmit and receive** functionally at the **same time**, i.e. during off-periods in OOK
  - need to predict LOW symbols in opposite LED
- cost-effective
- suited to applications where energy and form-factor are key

Same-time TX and RX can implement **CSMA/CD-HA** (Carrier Sensing Multiple Access/Collision Detection & Hidden Avoidance): a node $A$ will stop transmitting to another node $B$ if it receives HIGH-HIGH (meaning it's receiving from multiple nodes, creating collisions). $B$ can then detect that there is collision due to hidden node at $A$ when $A$ stops transmitting back. The reduced collision rate can improve throughput up to 300%.


**LED-to-camera**

*Rolling-shatter* problem:
  - need to **minimize exposure time** when image is taken row by row, else it will arrive distorted
  - optimize number of rows
  - account for the effect of distance, which results in smaller images $\rightarrow$ higher noise


**Passive communication**

Active VLC has good bandwidth but considerable energy cost. An alternative is to achieve passive communication:

- using **passive light**, e.g. the sun. The main devices that implements this idea are **LCDs** (Liquid Crystal Shutters).
- using **backscattering**: use either solar or artificial light to reflect signal. This is already done in reflective roadsigns, but they could be updated to convey additional (dynamic) information.

**Screen-to-camera**: e.g. double frame rate of a movie, encode information by mixing colors. Very new.

**Localization using LEDs**: LEDs can be used for fairly accurate localization.


# Modern WSN technologies

Nodes have very limited computational resources, both for reducing cost but also to minimize energy consumption as much as possible. Most of the power is indeed spent for communication:

- sensing: 15%
  - actuators
  - software
- computing: 25%
  - memory
  - microcontroller
  - OS
- **communication**: 60%
  - network ops
  - MAC
  - routing
  - SYNC

Additionally, nodes spend most of the time in sleep mode, which consume up to 1/3 of idle consumption.
Nodes can follow a **duty cycle**, but this has challenges in the form of latency and synchronization.

**Mobile IoT**: networked systems with mobile elements, particularly:

- Wireless Sensor Networks (WSN)
  - sensor nodes
- Unmanned Aircraft System (UAS)
  - UAV/drones


## Energy harvesting

In many applications, the network is required to run for **decades** without the possibility of replacing battery (a process which is very expensive). On the other hand, typical 2xAA batteries can allow a sensor no more than *a few days* if always on.
When **duty-cycling** with a very low % of uptime, such as CTP with LPL, we can reach a lifetime of slightly more than 1 year, at the cost of a **latency** as high as 10s, which is still too short for many use cases.

To prolong the lifetime of the system, a possibility is to exploit external energy sources, i.e. implement **energy harvesting** (e.g. solar or wind power) in the network. Theoretically, it is possible to design **energy neutral** systems, meaning they would never run out of battery. In practice, this is never the case because existing batteries are not 100% efficient (there is some leakage), but results are promising nonetheless.

### Short term predictions (Pro-Energy)

Energy harvesting, however, poses its own set of challenges due to uncertainty of energy availability (sun, wind), which means the system should be able to **predict** availability and utilize it to **plan energy allocation** accordingly, in order to minimize waste, since physical buffers (capacitors) are limited in practice. In particular, the system should try to spend any **energy surplus**, i.e. energy that can be harvested from nodes which already have a full energy buffer, potentially even using otherwise less-optimal routes.

Predictions are usually obtained by **profiling** typical days according to their weather condition, in order to obtain a *trace* which can approximate future days of similar weather condition. On future days, the system can then look through the profiles to find the most suited to the coming hours according to the measurements taken that far, adjusted by the mean error ^[check details?] (i.e. difference from the actual measurements of the day):

$$ \hat E_{t+1} = \alpha C_t + \left( 1- \alpha \right) E^d_{t+1}$$

where:

- $\hat E_{t+1}$ is the predicted energy intake in timeslot $t+1$ of current day
- $E^d_{t+1}$ is the energy harvested in timeslot $t+1$ of profiled day $d$
- $C_t$ is the energy harvested during timeslot $t$ on current day $C$
- $0 \le \alpha \le 1$ is a weighing factor (?)

Additionally, an algorithm for deciding whether to replace a profile with the newly recorded day, when memory is full, is needed, e.g.:

- discard profiles older than some $D$ days (prefer **temporal proximity**)
- discard the most similar profile to the incoming one among the **pairs of profiles** that are **too similar** ^[by Mean Absolute Error] (prefer **representativity**)
- consider combining multiple profiles into a "synthetic" one (try to predict yet unseen *hybrid days*)

### Medium term predictions

Data gathered for long periods of time can also be used to determine how much the measurements are representative of the future as a function of time passed (**Pearson autocorrelation**), which is something that varies greatly between the kinds of energy sources. For example, profile correlation decades much slower with time for solar power than for wind.

[more?]

### Harvesting-aware routing

To fully exploit the potential of sensor networks with energy harvesting (**EH-WSN**), harvesting-aware routing protocols are a necessity.
A first idea is to utilize this as a factor in **timer-based contentions**: respond to RTSs with a random jitter computed by taking into account harvesting rate, energy reservoir and hop count, and prioritizing (=shortest jitter) any node with energy peaks.

Additionally, it is possible to assign different kind of tasks to nodes in different conditions, e.g. try to cover a target area by only activating sensing in sensors that are currently harvesting energy. To accomplish this, sensing tasks (**missions**) can be scheduled in the network and then taken in by willing nodes.
Each missions has a given priority (**profit**) and requires a given amount of resources (**demand**); each node has a certain **utility** (quality of information) related to each possible task. In this way, each node will then make **autonomous decisions** about whether it can contribute to any given mission depending on mission profit, potential individual contribution, current energy level, predicted cost and target network lifetime. Willing nodes will then **bid** on the mission.

In particular, missions can be classified (according to their energy cost, and taking energy predictions into account) in:

- **battery required**: sustained at least partially by battery/fuel cell
- **capacitor-sustainable**: sustained by supercapacitator
- **recoverable**: sustained by supercapacitator, expense recoverable before next mission 
- **free**: fully sustained by harvesting

Expected **partial profit of a missions**:

$$ \overline{p} = \frac{E[u]}{E[d]}\cdot \frac{E[p]}{P}$$

where:

- $P$ is the maximum achievable profit
- $E[u]$ is expected utility of the mission
- $E[d]$ is expected demand
- $E[p]$ is expected profit

Partial **profit achievable by a participating node** in the mission, where $w$ depends on mission classification:

$$p^* = \frac{e_{ij}}{d_j} \cdot \frac{p_j}{P} \cdot w$$

Bid if $p^* \ge \overline{p}$, i.e. if the node has average or better partial profit (modified by mission classification).

A framework for experimenting with energy harvesting solutions is the `GreenCastalia` simulator, based on `Castalia`/`OMNET++`.

## Wake-up radios

A recent technology that can considerably prolong the lifetime of WSNs is the **wake-up radio**, a paradigm where nodes are woken only *on-demand*. This is done by using an additional, **low power transceiver**, composed of a WuTx and WuRx, which is active even during sleep and that can only receive a specific transmission (**wakeup call**, WuC), upon which the **main radio** is activated. This almost completely eliminates idle time on the main antenna, while achieving *better* latency than duty cycled systems.
Each node's special antenna has a specific wake-up address so that nodes can be selectively woken through WuCs as needed (**semantic awakening**).

For example, this can be used to selectively wake up nodes that have a certain amount of remaining battery with a drone-mounted radio signal, in order to collect their data.
This means that it is possible to deploy a **non-connected WSN** as a UAS.

It can also be used to only wake up a set nodes that dynamically changes over time according to local properties, like sensed humidity below a certain threshold (**semantic addressing**).

Additionally, it is possible to design **passive** WURs, which actually harvest power *from the radio signal* of the WuC and use it to answer, potentially removing the need for external power supplies (shifting energy toll completely to the transmitter ^[unsuited for P2P networking, but great for e.g. drone-assisted WSNs]). This however is done using RFID technology, therefore it requires very short range (~3m) and is prone to interference.

### Implementations

**Flood-WUP**: 

1. nodes are programmed with two different **wake-up addresses** $w_a,w_b$. 
2. initially all nodes in sleep, WuRx listening on $w_a$
3. the sink broacasts a WuC to address $w_a$, then intended packet
4. all nodes in range switch on the main radio to RX, receive packet
5. they switch WuRx to address $w_b$
6. they rebroadcast the packet after WuC $w_a$, to **prevent** node from receiving **duplicates**


**Green-WUP**: 

- **harvesting-aware** routing
- main idea: advance information towards sink by waking up only **good potential relays** through **semantic addressing**
- nodes dynamically change address to reflect their current state
- addressing format for WuC includes **hop-count** and **energy class** (which depends on residual energy and predicted intake):
  1. send RTS only to nodes with lower than current hopcount, and only best energy class (at max).
  2. If no answer within a time window, lower requested energy class.

This can reduce energy expense by up to ~96% compared to flooding with always-on nodes, and by up to ~33x compared to CTP without LPL at the cost of just ~100ms of latency. If also consumes *less* than CTP with very short duty cycle (e.g. 1%), while having a **vastly lower latency**.

**ALBA-WUR**: ALBA with additional selection criteria based on energy class.

In short, WUR can **reduce energy consumption** by up to **three orders of magnitude** with very **low latency cost** (comparable to always-on ALBA-R).


# Reinforcement learning for IoT

Types of machine learning:

- supervised
- unsupervised
- reinforcement: learn an optimal strategy to maximize a **cumulative** numeric reward function/signal

**Reinforcement learning**:

- No instantaneous feedback
- Time-sensitive
- Originally used for animal learning
- Each decision impacts data received after it

Reward is **cumulative**: the goal is to maximize the *total* reward across all actions, not just short-term. However, future rewards are usually slightly devalued over time compared to immediate reward, by a **discount factor** $0<\gamma<1$. This models situations where there is nonzero $(1-\gamma)$ probability of termination at any time, and is required to guarantee a finite sum without absorbing states.

Components:

- **policy**: defines the agent's behaviour. It is represented as a **map** from each state to a respective *good* action (**transition**)
- **reward signal**: defines the goal of the RL model, since the agent objective is to maximize the reward.
- **value function**: specifies what is *good* in the long run, i.e. $V^{\pi} (s) =\sum^\infty_{t=0} \gamma^t r(t)$ ^[known as **infinite horizon** formulation]
- **model**:: mimics behaviour of the real RL environment

## Markov Decision Processes (MDP)

We focus on discrete MDPs.

- MDP is a generalization of **Markov chains** where multiple actions are possible and there is a reward function which allows to choose among them
- environment is **fully observable**:
  - **Markovian property**: next state is a function of current state and action taken
  - **Memoryless**: does *not* depend on previous history
- IF there are **absorbing states**, cumulative reward may be *finite* even without discount
- policy is stochastic
- can formalize most RL problems

Structure:

- Finite set of states $S$, actions $A(s\in S)$
- **reward** function $R: S\times A \rightarrow R$ 
- **transition** function $T: S \times A \rightarrow S$
- **objective**: optimal policy $\pi^*: S \rightarrow A$ s.t. $\forall \pi,s \quad V^*\equiv V^{\pi^*}(s) \ge V^\pi(s)$

Agent and environment interact over a sequence of (discrete) time steps. Each step, the agent makes a decision on which action to take based on current state and evaluates the choice it made according to the reward function.

If S is discrete and A(s) finite ^[or a *compact* set], the existance of an optimal policy $\pi*$ can be proven, and it is indifferent of initial state $s_0$.
Theoretically, the optimal policy may be found by evaluating all possible policies, but this takes exponential time.
**Dynamic programming** approach: determine optimal value function $V^*(s)$ for each state $s$, then select actions according to it.

When the problem is completely defined (**offline learning**), $V^*(S)$* can be computed useing **Bellman equations**:

$$V^*(s) = \max_a\big\{R\left(s,a)\right)+\gamma V^*\left(T\left(s,a\right)\right)\big\}$$

By using *recurrence relations*, we obtain:

$$V^*(s) = \max_a\big\{R\left(s,a)\right)+\gamma \sum_{s'} P_{ss'}(a) V^*\left(T\left(s'\right)\right)\big\}$$

where $R(s,a) \equiv E\left[R | s,a\right]$, $P_{ss'}$ is the probability that the next state is $s'$ given that $a$ is chosen in $s$.

Therefore, **optimal policy** cna be computed by:

$$\pi^*(s) = \argmax_a \big\{ R(s,a) + \gamma V^*(s')  \big\}$$

However, in reality the model is often only partially known (*online learning*). In this case, the agent tries to maximize the **estimated utility function** $Q$, which includes the immediate reward for an action, plus the best possible utility for the resulting state (*future utility*).

- Q-learning, unlike Bellman Equations, can only be computed as a function of a specific initial action $a_0$.
- Q-learning does not strictly specify what the agent should do. Each *epoch*, the agent can either:
  -  *exploit past knowledge*, i.e. select currently estimated best action for the given state 
  -  *explore an alternative* to its currently estimated *best action* in order to build a better estimate of the optimal Q-function
- Guaranteed to converge to optimal Q-values only if all Q(s,a) are updated infinitely often

[more details and equations...]

### WHARP

A forwarding strategy to optimize **over time** energy consumption through a MDP-based selection of next-hop relays, utilizing:

- wake-up radio
- semantic addressing based on distance from sink
- nodes decide whether to participate in relay selection process based on a MDP considering available energy & predicted intake

**Backward Value Iteration** (BVI): [?]



# Blockchain technology
 
**Idea**: first to solve a puzzle gets a reward. Each puzzle depends on previous puzzle.

Each puzzle is hard to solve but easy to verify (NP problem?).

## Hash functions

Convert any object that can be represented by a string into a **fixed length** string. 

A cryptographic hash function has additional requirements:

- Must be **deterministic** to be useful 
- Must be quick to compute
- **Pre-image resistance**: must be hard to **invert**
- **Avalanche effect**: even small changes in input should produce large changes in output (output should *appear* random)
- **Puzzle friendly**: (?) concatenation can be used to obtain completely different outputs

If a particular hash is desired (e.g. beginning or terminating in some combination of characters), can concatenate some random number to the input string until the matching hash is of the desired type. The number that produces the correct result is called **nonce**.
The longer the target sequence, the harder it is to find the nonce, though verifying solution is still easy.

To obtain a new puzzle depending on the previous one, can concatenate the previous digest for the solution to the string, then find the nonce and matching digest for this new input string. This operation can be chained multiple times.

## Blockchain

A **blockchain** is a linked list of hash-puzzle solutions and it uses **hash pointers**, i.e. each block contains the hash of the previous block, hash of the current block (called **proof of work** in blockchain), and other deired information such as **transaction information**. 

This chain of blocks can be saved locally (**decentralization**) by each participant in puzzle-solving: the first to find the working nonce gets to store the block (this is a *coin*).

No one can change information in any given block without ruining the entire chain after that block, thereby being detected. This means that the transaction history is **immutable**.

The blockchain is also transparent, meaning everyone can follow back the chain to previous owners.

Attempting to find the current **proof of work** is called **mining** and it is competitive. 

Key feature is the **asymmetry**: finding the proof of work is hard and can only be brute forced (as long as cryptographic hash functions are used), but the system can easily verify that a proposed solution is working, and then **reward** the participant who computed the correct solution.

## Merkle tree

A binary tree where each node contains the hash of its 2 children. This means a node can download only the header of a given block from a source, then any relevant subtree (i.e. containing requested transactions) from another source, and verify that the data in it is authentic.
In fact, if a malicious user attempt to introduce a fake transaction at the bottom of the tree, this will change the hashes of all the block up to the root of the tree, so that the protocol will register this block as another block entirely.

## Bitcoin

First implementation of the blockchain idea using:

- a decentralized p2p network (**bitcoin protocol**)
- public transaction ledger (the actual blockchain)
- distributed mining (decentralized mathematical, deterministic currency issuance)
- transaction script (decentralised verification system)

**Transaction** process:

1. When a user request a transaction, this is broadcast to the P2P network
2. The network validates the transaction and its requensting user's status using **public algorithms**. The transaction cna involve **cryptocurrency**, contracts or other information.
3. The transaction is combined with past transaction to create a new block for the ledger, which is permanent.

The reward for mining comes from a small fee collected from the transaction itself.

A **block header** contains:

- A magic number
- protocol version number
- timestamp
- difficulty target for the puzzle, i.e. number of 0s required
- hash of previous block
- nonce
- Merkle root

The **difficulty** is periodically adjusted so that a solution is found, on average, once per 10 minutes.

**History** of mining:

- In 2009, cheap CPUs could turn out a profit due to small amount of people mining.
- In 2010, GPU started being used (much more efficient for large scale computations). 
- In 2011, dedicated hardware miners were developed: **FGPA**, needed to be programmed before use.
- In 2013, dedicated devices which were preloaded with necessary code to mine were introduced (**ASIC**)
- After 2013, *mining groups* started to aggregate computing power for higher efficiency, sharing any earned reward among its participants. A single user is completely priced out of profitably mining alone.


### Possible attacks

**Double spending attack**: attacker generates a transaction, then after verification produce new transaction paying the same amount to himself (or accomplice). Then attempt to persuade network that second transaction actually came first, rendering the first payment an orphaned block, thus invalid.
This is an attempt to exploit the part of the system not covered by cryptography (order of transactions).

To succeed in this, attacker has to be able to "rewrite history", by *forking* the blockchain, mining on the new fork, and then passing the fork as the authentic chain since the longest valid chain is assumed to be true. This can only be done if the attacker has more computational power than the entire rest of the network (51%), which is why it is imperative that the network stays **distributed**.

## Ethereum

Another blockchain technology which implements the concept of **smart contracts**, i.e. code with arbitrary rules controlling the digital assets on the blockchain.
In Ethereum's chain, each node stores the **most recent state** of each smart contract, plus all ether transactions. The network needs to keep track of the state of each application, meaning each user's balance, all the contract code and its location.

1. A user offers a good/service through publishing a smart contract: it contains the user's blockchain address and the terms of the sale.
2. Goods are stored in the network with a dedicated public key and access controlled by the contract.
3. A buyer signs the contract with his private key by authorizing a transaction with the specified amount of currency from her public address (which is a public key) to the seller's.
4. The signer can now use his private key to open the "smart lock" on the purchased goods.
5. The smart contract is verified by the network: if valid (the seller is recognized by consensus as the owner of the product, and the buyer has enough currency to pay), then ownership of the good is transferred to the buyer, and the currency to the seller.

## Proof of stake

A more efficient idea than the **proof of work** used by BitCoin, the idea is that the creator of a new block is chosen in a **deterministic** way based on its wealth, also called **stake**.
A proposed algorithm for this is `Casper`, which includes a validator pool that users can join.

## IOTA

IOTA foundation is a non-for-profit decentralized organization, mainly sustained by donations, which maintains and promotes the **IOTA protocol**. 

It includes a distributed ledger and a **fixed number of tokens**.

Table: Network decentralization

network | access points to "truth" | sources of "truth" | single points of failure?
-|-|-|-
Centralized | single | single | yes 
Distributed | multiple | single but replicated | yes
Decentralized | multiple | democratized (consensus) | no

### The blockchain bottleneck


The biggest problem with previous blockchain technologies is that they rely on **monetary incentives** (the cryptocurrency) in order to work, which is a bottleneck as it in turns means each transaction has a fee, which is increasingly higher for transactions that need to be fast (since mining is very expensive). This may make small transactions not worth the fee, preventing the system from achieving its intended real world usefulness.
For example, in Bitcoin the blockchain is limited to a global capacity of ~7 transactions per second. Increasing size or frequency of blocks is not possible as it will have the side effect of producing too many *forks*, which cannot be **reconciled** easily by the protocol.

### Tangle

Instead of a chain, IOTA uses a **tangle**, which is a particular kind of **DAG** ^[Directed Acyclic Graph] where each vertex is a transaction and each edge is an approval.
Transactions that are near the beginning of the cycle are confirmed by many more transactions, thus are considered to have high confidence. Newer transactions have no confidence until referenced.
Validating a transaction also indirectly further validates all transactions in the sub-graph of that transaction, increasing their confidence.
When a given confidence target is reached, the transaction is considered fully validated and becomes immutable.

In the tangle, nodes can work on multiple transactions **in parallel** and **cooperatively** rather than competitively, in order to have **no fees** on transactions, making IOTA suitable even for micropayments.
The motivation to verify transactions comes from the fact that to request a transaction be authorized, a user has to validate 2 existing transactions as well as offering their transaction to be the entry point of new transactions.
Validation of the ledger is however not free, but much simpler than in blockchain as it can be done up to any fully validated block (*milestone*) rather than the *genesis transaction* (which would require walking the entire graph).

Overall, this means that scaling the network (= the amount of transactions) also scales the speed at which transactions are referenced and ultimately validated.

**Main vulnerability**: in early IOTA history, it could have been possible to forge a sub tangele of forged transactions and then attempt to connect it to the real tangle, succeeding in "rewriting history" if the subtangle was big enough. However, this requires such a high portion of transactions of the main tangle that it is now unfeasable.
